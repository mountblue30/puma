import React, { Component } from 'react';
import dressOne from "../images/Dress1.jpg";

class SearchResultProduct extends Component {
    state = {  } 
    render() { 
        return (
          <div className="result-product">
            <img className="result-product-image" src={dressOne} />

            <div className="result-product-content">
              <div className="colors">3 colors</div>
              <div className="product-title-price">
                <p>HYBRID NX Men's Running Shoes</p>
                <h6>$1000</h6>
              </div>
              <div className="coupon">
                <p>Use code: MORE10 for extra 10% off</p>
              </div>
            </div>
          </div>
        );
    }
}
 
export default SearchResultProduct;