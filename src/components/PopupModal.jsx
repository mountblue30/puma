import React, { Component } from 'react';
import "../css/PopupModal.css"
import CheckoutCarousel from "./CheckoutCarousel";
import { Link } from "react-router-dom";

class PopupModal extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    render() { 
      console.log(this.props.productData);
        return (
          <>
            <div
              class="modal fade"
              id="staticBackdrop"
              data-bs-backdrop="static"
              data-bs-keyboard="false"
              tabindex="-1"
              aria-labelledby="staticBackdropLabel"
              aria-hidden="true"
            >
              <div class="modal-dialog  modal-dialog-centered " tabindex="-1">
                <div class="modal-content rounded-0">
                  <div className="modal-wrap">
                    <div class="modal-header">
                      <div className="check-icon">
                        <i class="fa-regular fa-circle-check"></i>
                      </div>
                      <h1 class="modal-title fs-5" id="staticBackdropLabel">
                        1 item(s) added to your cart!
                      </h1>
                      <button
                        type="button"
                        class="btn-close"
                        data-bs-dismiss="modal"
                        aria-label="Close"
                      ></button>
                    </div>
                    <div class="modal-body">
                      <div className="modal-body-sections">
                        <div className="modal-body-section-one">
                          <div className="modal-body-product">
                            <div className="modal-body-product-image-details">
                              <img
                                src={this.props.productData.image_primary}
                                alt=""
                              />
                              <div className="modal-body-product-details">
                                <div className="product-title">
                                  <strong>
                                    {this.props.productData.title}
                                  </strong>
                                </div>
                                <div className="other-details">
                                  <p>Color: {this.props.productData.color}</p>
                                  <p>Style: {this.props.productData.style}</p>
                                </div>
                              </div>
                            </div>
                            <div className="modal-body-product-price">
                              {this.props.totalWithCommas(this.props.cartTotal)}
                            </div>
                          </div>
                          <div className="modal-body-buttons">
                            <Link
                              to="/cart"
                              className="offcanvas-secure-checkout view-cart"
                            >
                              <span data-bs-dismiss="modal" aria-label="Close">
                                View Cart ({this.props.cart.length})
                              </span>
                            </Link>
                            <Link
                              to="/checkout"
                              className="offcanvas-secure-checkout"
                            >
                              <span data-bs-dismiss="modal" aria-label="Close">
                                CHECKOUT
                              </span>
                            </Link>
                          </div>
                        </div>
                        <div className="modal-body-section-two">
                          <div className="modal-body-banner">
                            <h5>
                              <strong>
                                GET AN EXTRA 5% OFF ON YOUR ORDER FOR ONLINE
                                PAYMENTS
                              </strong>
                            </h5>
                          </div>
                          <div className="modal-body-products">
                            <CheckoutCarousel />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        );
    }
}
 
export default PopupModal;