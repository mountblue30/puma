import React, { Component } from 'react';

import dressOne from "../images/Dress1.jpg";
import dressTwo from "../images/Dress2.jpg";
import dressThree from "../images/Dress3.jpg";
import "../css/Carousel.css"
class Carousel extends Component {
    state = {  } 
    render() { 
        return (
          <div
            id="carouselExampleControls"
            class="carousel slide"
            data-bs-ride="carousel"
          >
            <div class="carousel-inner">
              <div class="carousel-item">
                <div class="card-group">
                  <div class="card">
                    <img src={dressOne} class="card-img-top" alt="..." />
                    <div class="card-body ">
                      <h5 class="card-title">
                        <strong>PUMA Graphic Men's T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style.The Feel It Mesh Logo
                        Training T-Shirt is your go-to for breathable comfort
                        and style.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressTwo} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressThree} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item active">
                <div class="card-group">
                  <div class="card">
                    <img src={dressOne} class="card-img-top" alt="..." />
                    <div class="card-body ">
                      <h5 class="card-title">
                        <strong>PUMA Graphic Men's T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style.The Feel It Mesh Logo
                        Training T-Shirt is your go-to for breathable comfort
                        and style.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressTwo} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressThree} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="card-group">
                  <div class="card">
                    <img src={dressOne} class="card-img-top" alt="..." />
                    <div class="card-body ">
                      <h5 class="card-title">
                        <strong>PUMA Graphic Men's T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style.The Feel It Mesh Logo
                        Training T-Shirt is your go-to for breathable comfort
                        and style.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressTwo} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                  <div class="card">
                    <img src={dressThree} class="card-img-top" alt="..." />
                    <div class="card-body">
                      <h5 class="card-title">
                        <strong>Feel It Women's Mesh Logo T-Shirt</strong>
                      </h5>
                      <p class="card-text">
                        The Feel It Mesh Logo Training T-Shirt is your go-to for
                        breathable comfort and style. It features mesh inserts
                        at each sleeve plus a large swath of branded mesh across
                        the back, but the show-stopper is the bold PUMA graphic
                        across the front.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button
              class="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="prev"
            >
              <span
                class="carousel-control-prev-icon"
                aria-hidden="true"
              ></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button
              class="carousel-control-next"
              type="button"
              data-bs-target="#carouselExampleControls"
              data-bs-slide="next"
            >
              <span
                class="carousel-control-next-icon"
                aria-hidden="true"
              ></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        );
    }
}
 
export default Carousel;