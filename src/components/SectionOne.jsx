import React, { Component } from 'react';
import dressOne from "../images/Dress1.jpg"
import dressTwo from "../images/Dress2.jpg";
import dressThree from "../images/Dress3.jpg";
import dressFour from "../images/Dress4.jpg";
import dressFive from "../images/Dress5.jpg";
import dressSix from "../images/Dress6.jpg";


import "../css/SectionOne.css"
class SectionOne extends Component {
    render() { 
        return (
          <div className="section-one">
            <div className="images">
              <img src={dressOne} />
              <img src={dressTwo} />
              <img src={dressThree} />
              <img src={dressFour} />
              <img src={dressFive} />
              {/* <img className='imageSix' src={dressSix} /> */}
            </div>
            <div className="flauntHits">
              <h1>FLAUNT LATEST FITS</h1>
              <h3>NEW ARRIVALS</h3>
              <div className="shopping-category">
                <a >SHOP MEN</a>
                <a >SHOP MEN</a>
              </div>
            </div>
          </div>
        );
    }
}
 
export default SectionOne;