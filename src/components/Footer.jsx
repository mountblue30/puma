import React, { Component } from 'react';
import "../css/Footer.css"
import logo from "../images/pngwing.com_white.png";


class Footer extends Component {
    render() { 
        return (
          <>
            <div className="footer">
              <img src={logo} alt="Bootstrap" width="49" height="49" />
              <span>© PUMA SE, 2021. All Rights Reserved</span>
            </div>
          </>
        );
    }
}
 
export default Footer;