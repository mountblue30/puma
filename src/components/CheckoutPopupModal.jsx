import React, { Component } from "react";
import "../css/PopupModal.css";
import { Link } from "react-router-dom";

class PopupModal extends Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    console.log(this.props.productData);
    return (
      <>
        <div
          class="modal fade"
          id="staticBackdrop"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabindex="-1"
          aria-labelledby="staticBackdropLabel"
          aria-hidden="true"
        >
          <div class="modal-dialog  modal-dialog-centered " tabindex="-1">
            <div class="modal-content rounded-0">
              <div className="modal-wrap">
                <div class="modal-header">
                  <Link to="/">
                    <button
                      type="button"
                      class="btn-close"
                      data-bs-dismiss="modal"
                      aria-label="Close"
                      onClick={()=>{
                        this.props.clearCart()
                        const section = document.querySelector("#scrollTo");
                        section.scrollIntoView({
                          behavior: "smooth",
                          block: "start",
                        });
                      }}
                    ></button>
                  </Link>
                </div>
                <div class="modal-body">
                  <div className="modal-body-sections-order-placed">
                    <div className="check-icon-order-placed">
                      <i class="fa-solid fa-circle-check tick-icon"></i>
                    </div>
                    <div className="order-placed">
                      <h5>
                        <strong>Order placed successfully</strong>
                      </h5>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default PopupModal;
