import React, { Component } from 'react';
import whiteLock from "../images/white-lock.png";
import dressOne from "../images/Dress1.jpg";
import OffcanvasCartProduct from './OffcanvasCartProduct';
import "../css/CheckoutCart.css"
class CheckoutCollapse extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    render() { 
        return (
          <div>
            <div
              class="collapse-vertical collapse show"
              id="collapseWidthExample"
            >
              <div class="card card-body w-100 border-0">
                <div className="card-body-section-one">
                  <div className="checkout-cart-products">
                    {this.props.cart.map((product) => {
                      return (
                        <OffcanvasCartProduct
                          product={product}
                          updateQuantity={this.props.updateQuantity}
                          deleteItem={this.props.deleteItem}
                        />
                      );
                    })}
                  </div>
                  <div
                    class="accordion accordion-flush"
                    id="accordionFlushExample"
                  >
                    <div class="accordion-item">
                      <h2 class="accordion-header" id="flush-headingOne">
                        <button
                          class="accordion-button collapsed accor"
                          type="button"
                          data-bs-toggle="collapse"
                          data-bs-target="#flush-collapseOne"
                          aria-expanded="false"
                          aria-controls="flush-collapseOne"
                        >
                          Apply Promo Code
                        </button>
                      </h2>
                      <div
                        id="flush-collapseOne"
                        class="accordion-collapse collapse"
                        aria-labelledby="flush-headingOne"
                        data-bs-parent="#accordionFlushExample"
                      >
                        <div class="accordion-body accor">
                          <h5>Enter Promo Code</h5>
                          <input
                            type="text"
                            className="form-control me-1 p-2"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="cart-checkout">
                      <div className="shipping-cost">
                        <p>Subtotal</p>
                        <h6>
                          {this.props.totalWithCommas(this.props.cartTotal)}
                        </h6>
                      </div>
                      <div className="shipping-cost">
                        <p>Shipping costs</p>
                        <h6>FREE</h6>
                      </div>

                      <hr />
                      <div className="grand-total">
                        <div className="total">
                          <div className="total-title">
                            <h3>Grand Total</h3>
                            <span>Prices include GST</span>
                          </div>
                          <div className="price">
                            <strong>
                              {this.props.totalWithCommas(this.props.cartTotal)}
                            </strong>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
    }
}
 
export default CheckoutCollapse;