import React, { Component } from 'react';
import whiteLock from "../images/white-lock.png";

class CheckoutSectionTwo extends Component {
    constructor(props) {
        super(props);
        this.state = {
          payment:""
        }
    }
    state = {  }
    submit = ()=>{
      this.props.flipClass("part-two", "submit");
    }
    handleRadio = (value)=>{
      this.setState({
        payment:value
      })
    }
    render() { 
        return (
          <div className="section-one-part-two">
            <div className="section-one-part-two-header">
              <h5>2. PAY</h5>
              <a
                className={this.props.pay}
                onClick={() => {
                  this.props.flipClass("part-two", "edit");
                }}
              >
                EDIT
              </a>
            </div>
            <div className={this.props.paymentChecks}>
              <form>
                <label class="radio-inline">
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("UPI");
                    }}
                  />
                  <i></i>
                  <span>UPI</span>
                </label>
                <label class="radio-inline">
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("Credit Card");
                    }}
                  />
                  <i></i>
                  <span>Credit Card</span>
                </label>
                <label>
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("Online Banking India");
                    }}
                  />
                  <i></i>
                  <span>Online Banking</span>
                </label>
                <label>
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("PayU");
                    }}
                  />
                  <i></i>
                  <span>PayU</span>
                </label>
                <label>
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("Cash On Delivery");
                    }}
                  />
                  <i></i>
                  <span>Cash On Delivery</span>
                </label>
                <label>
                  <input
                    type="radio"
                    name="opinion"
                    onChange={() => {
                      this.handleRadio("Simpl");
                    }}
                  />
                  <i></i>
                  <span>Simpl</span>
                </label>
              </form>

              <a
                className="offcanvas-secure-checkout"
                onClick={() => {
                  this.submit();
                }}
              >
                CONTINUE TO REVIEW AND PAY
              </a>
            </div>
            <div className={this.props.sectionTwoContent}>
              <div className="column-one-part-one-header">
                <strong>Payment Method:</strong>
              </div>
              <div className="column-one-part-one-content">
                <p>{this.state.payment} - Extra 5% off</p>
                <p>Amount: 762</p>
              </div>
            </div>
          </div>
        );
    }
}
 
export default CheckoutSectionTwo;