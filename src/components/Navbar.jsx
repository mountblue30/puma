import React, { Component } from "react";
import pumaLogo from "../images/pngwing.com_white.png";
import "../css/Navbar.css";
import searchLogo from "../images/search-white.png";
import cartLogo from "../images/cart-white.png";
import userLogo from "../images/user.png";
import { Link } from "react-router-dom";
import OffcanvasCart from "./OffcanvasCart";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: "",
    };
  }
  setSearchTerm = (event)=>{
    this.setState({
      searchTerm: event.target.value,
    })
  }
  render() {
    return (
      <>
        <nav className="fixed-top" id="fixed-top">
          <div className="nav-part-one">
            <div className="logo">
              <Link to="/">
                <img
                  src={pumaLogo}
                  alt="Bootstrap"
                  width="40"
                  height="40"
                  onClick={() => {
                    const section = document.querySelector("#scrollTo");
                    section.scrollIntoView({
                      behavior: "smooth",
                      block: "start",
                    });
                  }}
                />
              </Link>
            </div>
            <div className="categories desktop">
              <Link
                to="/new-arrivals"
                onClick={() => {
                  const section = document.querySelector("#scrollTo");
                  section.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                  });
                }}
              >
                New arrivals{" "}
              </Link>
              <a href=""> Kids </a>
              <a href=""> Collabrations </a>
              <a href=""> Sports </a>
              <a href=""> Outlets </a>
            </div>
            <div className="categories mobile">
              <Link
                to="/new-arrivals"
                onClick={() => {
                  const section = document.querySelector("#scrollTo");
                  section.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                  });
                }}
              >
                New arrivals{" "}
              </Link>
            </div>
          </div>

          <div className="nav-part-two">
            <div className="search-bar">
              <Link
                to="/search"
                onClick={() => {
                  const section = document.querySelector("#scrollTo");
                  section.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                  });
                }}
              >
                <div
                  onClick={() => {
                    this.props.onSearch(this.state.searchTerm);
                  }}
                >
                  <img src={searchLogo} alt="" height="25" />
                </div>
              </Link>
              <input
                className="form-control me-1 bg-transparent text-white p-2"
                type="search"
                placeholder="SEARCH PUMA.COM"
                aria-label="Search"
                onChange={(event) => {
                  this.setSearchTerm(event);
                }}
              />
            </div>
            <div className="cart-logo">
              <span tooltip="View cart" flow="down">
                <a
                  data-bs-toggle="offcanvas"
                  href="#offcanvasExample"
                  role="button"
                  aria-controls="offcanvasExample"
                >
                  <img src={cartLogo} alt="" height="29" />
                </a>
              </span>
              {/* <Link
                to="/login"
               
              >
                <img src={userLogo} alt="" height="29" />
              </Link> */}
              <div class="dropdown">
              <a class="btn rounded-0 dropdown-toggle bg-transparent" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
               <img src={userLogo} alt="" height="29" />
              </a>

              <ul class="dropdown-menu">
                {!this.props.loggedIn && (
                  <li><Link to="/login" class="dropdown-item">Login</Link></li>
                )}
                {this.props.loggedIn && (
                  <li><a  class="dropdown-item" onClick={()=>{
                    this.props.logOut()
                  }}>Logout</a></li>
                )}

              </ul>
            </div>
            </div>
            <div className="categories mobile-420">
              <Link
                to="/new-arrivals"
                onClick={() => {
                  const section = document.querySelector("#scrollTo");
                  section.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                  });
                }}
              >
                New arrivals
              </Link>
            </div>
          </div>
        </nav>

        <OffcanvasCart
          cart={this.props.cart}
          updateQuantity={this.props.updateQuantity}
          deleteItem={this.props.deleteItem}
          cartTotal={this.props.cartTotal}
          totalWithCommas={this.props.totalWithCommas}
        />
      </>
    );
  }
}

export default Navbar;
