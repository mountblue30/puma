import React, { Component } from 'react';
import CartProduct from './CartProduct';
import "../css/Cart.css"

import whiteLock from "../images/white-lock.png";
import emptycart from "../images/emptycart.jpg";
import { Link } from "react-router-dom";

import notLoggedIn from "../images/notLoggedIn.jpg";


class Cart extends Component {
    constructor(props){
      super(props)
    }
    render() { 
        return (
          <div className="my-cart">
            {this.props.loggedIn && (
              <>
                <div className="cart-title">
                  <h2>MY SHOPPING CART ({this.props.cart.length})</h2>
                </div>
                <p>
                  <i class="fa-solid fa-rotate-right"></i> Free and easy returns
                  on all orders.
                </p>
                {this.props.cart.length !== 0 && (
                  <div className="cart-parts">
                    <div className="cart-part-one">
                      {this.props.cart.map((product) => {
                        return (
                          <CartProduct
                            product={product}
                            updateQuantity={this.props.updateQuantity}
                            deleteItem={this.props.deleteItem}
                          />
                        );
                      })}
                    </div>
                    <div className="cart-part-two">
                      <div className="cart-container">
                        <div
                          class="accordion accordion-flush"
                          id="accordionFlushExample"
                        >
                          <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                              <button
                                class="accordion-button collapsed accor"
                                type="button"
                                data-bs-toggle="collapse"
                                data-bs-target="#flush-collapseOne"
                                aria-expanded="false"
                                aria-controls="flush-collapseOne"
                              >
                                Apply Promo Code
                              </button>
                            </h2>
                            <div
                              id="flush-collapseOne"
                              class="accordion-collapse collapse"
                              aria-labelledby="flush-headingOne"
                              data-bs-parent="#accordionFlushExample"
                            >
                              <div class="accordion-body accor">
                                <h5>Enter Promo Code</h5>
                                <input
                                  type="text"
                                  className="form-control me-1 p-2"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="cart-checkout">
                            <div className="shipping-cost">
                              <p>Subtotal</p>
                              <h6>
                                {this.props.totalWithCommas(
                                  this.props.cartTotal
                                )}
                              </h6>
                            </div>
                            <div className="shipping-cost">
                              <p>Shipping costs</p>
                              <h6>FREE</h6>
                            </div>

                            <hr />
                            <div className="grand-total">
                              <div className="total">
                                <div className="total-title">
                                  <h3>Grand Total</h3>
                                  <span>Prices include GST</span>
                                </div>
                                <div className="price">
                                  <strong>
                                    {this.props.totalWithCommas(
                                      this.props.cartTotal
                                    )}
                                  </strong>
                                </div>
                              </div>

                              <div className="checkouts">
                                <Link
                                  to="/checkout"
                                  className="offcanvas-secure-checkout"
                                >
                                  <img src={whiteLock} height="20" />
                                  CHECKOUT
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {this.props.cart.length === 0 && (
                  <div className="empty-cart">
                    <div className="empty-cart-title">
                      <h3>Cart is empty!</h3>
                    </div>
                    <div className="empty-cart-image">
                      <img src={emptycart} />
                    </div>
                  </div>
                )}
              </>
            )}
            {!this.props.loggedIn && (
              <div className="not-loggedin">
                <h3>Login to view cart.</h3>
                <div className="not-loggedin-image">
                  <img src={notLoggedIn} alt="" />
                </div>
              </div>
            )}
          </div>
        );
    }
}
 
export default Cart;