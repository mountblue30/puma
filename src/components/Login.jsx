import React, { Component } from 'react';
import validator from "validator";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      emailError: "",
      passwordError: "",
      loginError: "",
      loginSuccess: "",
      
    };
  }
  handleInputChange = (event) => {
    if (event.target.name === "email") {
      this.setState({
        email: event.target.value,
      });
    } else {
      this.setState({
        password: event.target.value,
      });
    }
  };
  validate = () => {
    let emailError = "";
    let passwordError = "";

    if (validator.isEmpty(this.state.email)) {
      emailError = "Email should not be empty";
    } else {
      if(!validator.isEmail(this.state.email)){
        emailError = "Provide a valid email address."
      }
    }
    if (validator.isEmpty(this.state.password)){
      passwordError = "Password should not be empty."
    }
      if (emailError || passwordError) {
        this.setState({ emailError, passwordError });
        return false;
      }
    this.setState({ emailError, passwordError});
    return true;
  };
  submit = () => {
    if(this.validate()){
      let result = this.props.getDetails(this.state)
      if(result["status"]){
        this.setState({
          loginSuccess: result["value"],
          loginError:"",
        },()=>{
          
          this.props.changeClass()
        });
      } else {
         this.setState({
           loginError: result["value"],
           loginSuccess:"",
         });
      }
      
    }
  };
  render() {
    return (
      <div className="login">
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="search"
          placeholder="EMAIL*"
          aria-label="Email"
          name="email"
          onChange={this.handleInputChange}
        />
        <div className="errorMsg">{this.state.emailError}</div>
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="password"
          placeholder="PASSWORD*"
          aria-label="Password"
          name="password"
          onChange={this.handleInputChange}
        />

        <div className="errorMsg">{this.state.passwordError}</div>

        <div class="form-check">
          <input
            class="form-check-input"
            type="checkbox"
            value=""
            id="defaultCheck1"
          />

          <label class="form-check-label" for="defaultCheck1">
            Remember me
          </label>
        </div>
        <div className="login-submit">
          <a
            className="btn login-button rounded-0 p-3"
            onClick={() => {
              this.submit();
            }}
          >
            LOGIN
          </a>

          <a className="btn forget p-3 rounded-0">FORGOT PASSWORD?</a>
        </div>
        <div className="errorMsg">{this.state.loginError}</div>
        {/* <div className="successMsg">{this.state.loginSuccess}</div> */}
      </div>
    );
  }
}
 
export default Login;