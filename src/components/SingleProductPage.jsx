import React, { Component } from "react";
import "../css/SingleProductPage.css"
import ProductMobileView from "./ProductMobileView";
import PopupModal from "./PopupModal";
import Carousel from "./Carousel";
import { Link } from "react-router-dom";

class SingleProductPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity:1
    }
  }
  updateQuantity = (quantity) => {
    this.setState({
      quantity: quantity
    })
  }
  render() {
    return (
      <>
        <div className="single-product-container">
          <div className="current-path-header">
            <h5>Home</h5>
            <p>.</p>
            <h5>Clothing</h5>
            <p>.</p>
            <h6>Category</h6>
            <p>.</p>
            <h6>{this.props.productData.title}</h6>
          </div>
          <div className="single-product-container-parts">
            <div className="single-product-container-part-one">
              <div className="desktop-view">
                <div className="image-primary">
                  <img src={this.props.productData.image_primary} alt="" />
                </div>
                <div className="image-placement">
                  <img src={this.props.productData.image_primary} alt="" />
                  <img src={this.props.productData.image_secondary} alt="" />
                  <img src={this.props.productData.image_secondary} alt="" />
                  <img src={this.props.productData.image_primary} alt="" />
                  <img src={this.props.productData.image_primary} alt="" />
                  <img src={this.props.productData.image_secondary} alt="" />
                </div>
              </div>
              <div className="mobile-view">
                <ProductMobileView productData={this.props.productData} />
              </div>
            </div>
            <div className="single-product-container-part-two">
              <div className="single-product-title">
                <h4>{this.props.productData.title}</h4>
              </div>
              <h5 className="single-price">{this.props.productData.price}</h5>
              <h6>Color : {this.props.productData.color}</h6>
              <div className="single-product-coupon">
                Use code: MORE10 for extra 10% off
              </div>
              {this.props.loggedIn && (
                <div className="add-to-cart">
                  <div class="dropdown">
                    <label>Quantity: </label>
                    <a
                      class="btn drop dropdown-toggle rounded-0 p-2"
                      href="#"
                      role="button"
                      data-bs-toggle="dropdown"
                      aria-expanded="false"
                    >
                      {this.state.quantity}
                    </a>

                    <ul class="dropdown-menu">
                      <li>
                        <a
                          class="dropdown-item"
                          name="1"
                          onClick={() => {
                            this.updateQuantity(1);
                          }}
                        >
                          1
                        </a>
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          name="2"
                          onClick={() => {
                            this.updateQuantity(2);
                          }}
                        >
                          2
                        </a>
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          name="3"
                          onClick={() => {
                            this.updateQuantity(3);
                          }}
                        >
                          3
                        </a>
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          name="4"
                          onClick={() => {
                            this.updateQuantity(4);
                          }}
                        >
                          4
                        </a>
                      </li>
                      <li>
                        <a
                          class="dropdown-item"
                          name="5"
                          onClick={() => {
                            this.updateQuantity(5);
                          }}
                        >
                          5
                        </a>
                      </li>
                    </ul>
                  </div>

                  <a
                    className="offcanvas-secure-checkout "
                    data-bs-toggle="modal"
                    data-bs-target="#staticBackdrop"
                    onClick={() => {
                      this.props.addToCart(
                        this.props.productData,
                        this.state.quantity
                      );
                    }}
                  >
                    ADD TO CART
                  </a>
                </div>
              )}
              {!this.props.loggedIn && (
                <Link
                  className="offcanvas-secure-checkout "
                  to="/login"
                >
                  Login to add to cart
                </Link>
              )}
              <div className="shipping-free">
                <div className="icon">
                  <i class="fa-solid fa-truck-fast"></i>
                </div>
                <p>This item ships free *</p>
              </div>
              <div className="pincode">
                <p>Please enter PIN code to check delivery time</p>
                <div className="enter-pin">
                  <input
                    className="form-control me-1 bg-transparent p-2 rounded-0"
                    type="text"
                    placeholder="PINCODE*"
                    name="pincode"
                  />
                  <a className="offcanvas-secure-checkout">CHECK</a>
                </div>
              </div>
              <div class="accordion" id="accordionExample">
                <div class="accordion-item rounded-0">
                  <h2 class="accordion-header" id="headingOne">
                    <button
                      class="accordion-button"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Description
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    class="accordion-collapse collapse show"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    <div class="accordion-body">
                      <p>{this.props.productData.description}</p>
                      <ul>
                        <li>Style: {this.props.productData.style}</li>

                        <li>Colour: {this.props.productData.color}</li>
                      </ul>
                      <a className="read-more" href="#description">
                        Read more
                      </a>
                    </div>
                  </div>
                </div>
                <div class="accordion-item rounded-0">
                  <h2 class="accordion-header" id="headingTwo">
                    <button
                      class="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Free Shipping and Returns
                    </button>
                  </h2>
                  <div
                    id="collapseTwo"
                    class="accordion-collapse collapse"
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample"
                  >
                    <div class="accordion-body">
                      <p>
                        Free standard delivery on all orders and free return
                        within
                        <strong>14 days of your order delivery date</strong>
                        .Visit our Return Policy for more information
                      </p>
                      <p>
                        For any queries, please contact Customer Service at
                        080-35353535 or via customercareindia@puma.com
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="also-bought">
            <div className="also-bought-title">
              <h3>CUSTOMERS ALSO BOUGHT</h3>
            </div>
          </div>
          <Carousel />
          <div className="single-product-details" id="description">
            <h3>Product Story</h3>
            <p>{this.props.productData.description}</p>
          </div>
        </div>
        <PopupModal
          cart={this.props.cart}
          productData={this.props.productData}
          cartTotal={this.props.cartTotal}
          totalWithCommas={this.props.totalWithCommas}
        />
      </>
    );
  }
}

export default SingleProductPage;
