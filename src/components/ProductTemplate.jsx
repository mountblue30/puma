import React, { Component } from "react";
import "../css/SearchResults.css";
import Products from "./Products";

class ProductTemplate extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <>
        <div className="results-found">
          <h1>{this.props.productData.length} PRODUCTS FOUND</h1>
        </div>
        <div className="result-products">
          {this.props.productData.map((product) => {
            return (
              <Products
                addToCart={this.props.addToCart}
                product={product}
                deleteItem={this.props.deleteItem}
              />
            );
          })}
        </div>
      </>
    );
  }
}

export default ProductTemplate;