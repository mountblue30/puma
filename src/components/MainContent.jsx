import React, { Component } from 'react';
import SectionOne from "./SectionOne";
import SectionTwo from "./SectionTwo";
import Carousel from "./Carousel";

class MainContent extends Component {
    state = {  } 
    render() { 
        return (
          <>
            <SectionOne />
            <SectionTwo />
            <Carousel />
          </>
        );
    }
}
 
export default MainContent;