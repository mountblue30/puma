import React, { Component } from 'react';
import validator from "validator";


class CheckoutSectionOne extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      postcode: "",
      addressOne: "",
      addressTwo: "",
      city: "",
      state: "STATE*",
      country: "COUNTRY",
      email: "",
      mobileNumber: "",
      alternativeMobileNumber: "",
      firstNameError: "",
      lastNameError: "",
      postcodeError: "",
      addressOneError: "",
      cityError: "",
      stateError: "",
      emailError: "",
      mobileNumberError: "",
      firstNameErrorMsg: "checkoutErrorMsg",
      lastNameErrorMsg: "checkoutErrorMsg",
      postcodeErrorMsg: "checkoutErrorMsg",
      addressOneErrorMsg: "checkoutErrorMsg",
      cityErrorMsg: "checkoutErrorMsg",
      stateErrorMsg: "checkoutErrorMsg",
      emailErrorMsg: "checkoutErrorMsg",
      mobileNumberErrorMsg: "checkoutErrorMsg",
    };
  }
  handleDropDown(key,value){
    if(key === "country"){
      this.setState({
        country: value,
      });
    } else {
      this.setState({
        state: value,
      });
    }
  }
  handleInputChange = (event) => {
    if(event.target.name === "firstName"){
      this.setState({
        firstName:event.target.value
      })
    }
    if (event.target.name === "addressOne") {
      this.setState({
        addressOne: event.target.value,
      });
    }
    if (event.target.name === "city") {
      this.setState({
        city: event.target.value,
      });
    }
    if (event.target.name === "state") {
      this.setState({
        state: event.target.value,
      });
    }
    if (event.target.name === "email") {
      this.setState({
        email: event.target.value,
      });
    }
    if (event.target.name === "mobileNumber") {
      this.setState({
        mobileNumber: event.target.value,
      });
    }
    if (event.target.name === "postcode") {
      this.setState({
        postcode: event.target.value,
      });
    }
    if(event.target.name === "lastName"){
      this.setState({
        lastName:event.target.value
      }) 
    }
  };
  validate = () => {
    let emailError = "";
    let firstNameError = "";
    let lastNameError = "";
    let postcodeError = "";
    let addressOneError = "";
    let cityError = "";
    let mobileNumberError = "";
    let stateError = "";
    let firstNameErrorMsg =  ""
    let lastNameErrorMsg= ""
    let  postcodeErrorMsg= ""
    let  addressOneErrorMsg= ""
    let  cityErrorMsg= ""
    let  stateErrorMsg= ""
    let  emailErrorMsg= ""
    let  mobileNumberErrorMsg= ""

    if (validator.isEmpty(this.state.email)) {
      emailError = "Email should not be empty";
      emailErrorMsg = "checkoutErrorMsg active";
    } else {
      if (!validator.isEmail(this.state.email)) {
        emailError = "Provide a valid email address.";
        emailErrorMsg = "checkoutErrorMsg active";

      }
    }
    if (validator.isEmpty(this.state.firstName)) {
      firstNameError = "First name should not be empty.";
      firstNameErrorMsg = "checkoutErrorMsg active";
    } else {
      if(!validator.isAlpha(this.state.firstName)){
        firstNameError = "First name should contain only alphabets."
        firstNameErrorMsg = "checkoutErrorMsg active";
      }
    }
    if (validator.isEmpty(this.state.lastName)) {
      lastNameError = "Last name should not be empty.";
      lastNameErrorMsg = "checkoutErrorMsg active";
    } else {
      if (!validator.isAlpha(this.state.lastName)) {
        lastNameError = "First name should contain only alphabets.";
        lastNameErrorMsg = "checkoutErrorMsg active";
      }
    }
    if (validator.isEmpty(this.state.postcode)) {
      postcodeError = "Postcode should not be empty.";
      postcodeErrorMsg = "checkoutErrorMsg active";
    } else {
      if(!validator.isNumeric(this.state.postcode)){
        postcodeError = "Postcode should consist only numbers.";
        postcodeErrorMsg = "checkoutErrorMsg active";
      } else {
        if (
          (this.state.postcode.length !== 6)
        ) {
          postcodeError = "Postcode should be only of 6 digits.";
          postcodeErrorMsg = "checkoutErrorMsg active";
        }
      }
    }
    if (validator.isEmpty(this.state.addressOne)) {
      addressOneError = "Primary address should not be empty.";
      addressOneErrorMsg = "checkoutErrorMsg active";
    }
    if (validator.isEmpty(this.state.city)) {
      cityError = "City should not be empty.";
      cityErrorMsg = "checkoutErrorMsg active";

    }
    if (this.state.state === "STATE*") {
      stateError = "State should not be empty.";
      stateErrorMsg = "checkoutErrorMsg active";

    }
    if (validator.isEmpty(this.state.mobileNumber)) {
      mobileNumberError = "Primary mobile number should not be empty.";
      mobileNumberErrorMsg = "checkoutErrorMsg active";
    } else {
      if(!validator.isNumeric(this.state.mobileNumber)){
        mobileNumberError = "Mobile number should only consist of numbers."
        mobileNumberErrorMsg = "checkoutErrorMsg active";
      } else {
        if(!validator.isLength(this.state.mobileNumber,{min:10,max:10})){
          mobileNumberError = "Mobile number should be only of 10 digits.";
          mobileNumberErrorMsg = "checkoutErrorMsg active";
        }
      }
    }
    if (emailError || firstNameError || lastNameError || cityError || stateError || mobileNumberError || postcodeError || addressOneError) {
      this.setState({
        emailError,
        firstNameError,
        lastNameError,
        cityError,
        stateError,
        mobileNumberError,
        postcodeError,
        addressOneError,
        firstNameErrorMsg,
        lastNameErrorMsg,
        postcodeErrorMsg,
        addressOneErrorMsg,
        cityErrorMsg,
        stateErrorMsg,
        emailErrorMsg,
        mobileNumberErrorMsg
      });
      return false;
    }
    this.setState({
      emailError,
      firstNameError,
      lastNameError,
      cityError,
      stateError,
      mobileNumberError,
      postcodeError,
      addressOneError,
      firstNameErrorMsg,
      lastNameErrorMsg,
      postcodeErrorMsg,
      addressOneErrorMsg,
      cityErrorMsg,
      stateErrorMsg,
      emailErrorMsg,
      mobileNumberErrorMsg
    });
    return true;
  };
  submit = () => {
    if (this.validate()) {
      this.props.flipClass("part-one","submit")
    }
  };
  
  state = {};
  render() {
    return (
      <div className="section-one-part-one">
        <div className="section-one-part-one-header">
          <h5>1. ADDRESSES</h5>
          <a
            className={this.props.address}
            onClick={() => {
              this.props.flipClass("part-one", "edit");
            }}
          >
            EDIT
          </a>
        </div>
        <div className={this.props.sectionOneInput}>
          <div className="input-part-one">
            <input
              className="form-control me-1 bg-transparent p-2 rounded-0"
              type="text"
              placeholder="FIRST NAME*"
              name="firstName"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.firstNameErrorMsg}>
              {this.state.firstNameError}
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="LAST NAME*"
              aria-label="Search"
              name="lastName"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.lastNameErrorMsg}>
              {this.state.lastNameError}
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="POST CODE*"
              name="postcode"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.postcodeErrorMsg}>
              {this.state.postcodeError}
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="ADDRESS 1*"
              name="addressOne"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.addressOneErrorMsg}>
              {this.state.addressOneError}
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="ADDRESS 2"
              name="addressTwo"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            {/* <div className=" checkoutErrorMsg">{this.state.lastNameError}</div> */}
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="CITY*"
              name="city"
              aria-label="Search"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.cityErrorMsg}>
              {this.state.cityError}
            </div>
            <div class="dropdown">
              <a
                class="btn drop dropdown-toggle w-100 d-flex  justify-content-between align-items-center rounded-0 drop-down-content"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                {this.state.state}
              </a>

              <ul class="dropdown-menu w-100">
                <li>
                  <a
                    class="dropdown-item drop-down-content"
                    name="tamilNadu"
                    // onClick={() => {
                    //   this.props.updateQuantity(this.props.product.id, 1);
                    // }}
                    onClick={() => {
                      this.handleDropDown("state", "TAMIL NADU");
                    }}
                  >
                    TAMIL NADU
                  </a>
                </li>
              </ul>
            </div>
            <div className={this.state.stateErrorMsg}>
              {this.state.stateError}
            </div>
            <div class="dropdown ">
              <a
                class="btn drop dropdown-toggle w-100 d-flex  justify-content-between align-items-center rounded-0 drop-down-content"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                {this.state.country}
              </a>

              <ul class="dropdown-menu w-100">
                <li>
                  <a
                    class="dropdown-item drop-down-content"
                    name="India"
                    //   onClick={() => {
                    //     this.props.updateQuantity(this.props.product.id, 1);
                    //   }}
                    onClick={() => {
                      this.handleDropDown("country", "INDIA");
                    }}
                  >
                    INDIA
                  </a>
                </li>
              </ul>
            </div>
          </div>
          {/* <div className=" checkoutErrorMsg">{this.state.lastNameError}</div> */}
          <div className="input-part-two">
            <div className="input-part-two-header">
              <h4>Enter Contact Info (for Order Invoice)</h4>
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="EMAIL*"
              name="email"
              aria-label="Search"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.emailErrorMsg}>
              {this.state.emailError}
            </div>
            <input
              className="form-control me-1 bg-transparent   p-2 rounded-0"
              type="text"
              placeholder="MOBILE NUMBER*"
              name="mobileNumber"
              aria-label="Search"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
            <div className={this.state.mobileNumberErrorMsg}>
              {this.state.mobileNumberError}
            </div>
            <input
              className="form-control me-1 bg-transparent p-2 rounded-0"
              type="text"
              placeholder="ALTERNATIVE MOBILE NUMBER"
              name="alternativeMobileNumber"
              aria-label="Search"
              onChange={(event) => {
                this.handleInputChange(event);
              }}
            />
          </div>
          <div className="input-part-three">
            <div className="input-part-three-header">
              <h4>Select a Shipping Method</h4>
            </div>
            <label>
              <input
                type="radio"
                name="opinion"
                checked
                onChange={() => {
                  this.handleRadio("Online Banking India");
                }}
              />
              <i></i>
              <span>Online Banking India</span>
            </label>
          </div>
          <a
            className="offcanvas-secure-checkout"
            onClick={() => {
              this.submit();
            }}
          >
            CONTINUE TO PAYMENT METHOD
          </a>
        </div>

        <div className={this.props.sectionOneContent}>
          <div className="content-columns">
            <div className="column-one">
              <div className="column-one-part-one">
                <div className="column-one-part-one-header">
                  <strong>Shipping Address:</strong>
                </div>
                <div className="column-one-part-one-content">
                  <p>
                    {this.state.firstName}
                    {this.state.addressOne} {this.state.postcode},
                    {this.state.city},{this.state.state},{this.state.country}
                  </p>
                </div>
              </div>
              <div className="column-one-part-two">
                <div className="column-one-part-two-header">
                  <strong>Shipping Method:</strong>
                </div>
                <div className="column-one-part-two-content">
                  <p>Standard - ₹0</p>
                </div>
              </div>
            </div>
            <div className="column-two">
              <div className="column-two-part-one">
                <div className="column-two-part-one-header">
                  <strong>Billing Address:</strong>
                </div>
                <div className="column-two-part-one-content">
                  <p>
                    {this.state.firstName}
                    {this.state.addressOne} {this.state.postcode},
                    {this.state.city},{this.state.state},{this.state.country}
                  </p>
                </div>
              </div>
              <div className="column-two-part-two">
                <div className="column-two-part-two-header">
                  <strong>Contact Info:</strong>
                </div>
                <div className="column-two-part-two-content">
                  <p>{this.state.email.slice(0, 20)}...</p>
                  <p>+91{this.state.mobileNumber}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
 
export default CheckoutSectionOne;