import React, { Component } from "react";
import "../css/LoginAndSignup.css";
import CreateAccount from "./CreateAccount";
import Login from "./Login"


class LoginAndSignup extends Component {
  constructor(props){
    super(props);
    this.state = {
      class:"alert alert-success hide"
    }
  }
  changeClass = ()=>{
    this.setState({
      class: "alert alert-success show",
    });
  }
  render() {
    return (
      <div className="login-container">
        <div className="login-header">
          <h3>MY ACCOUNT</h3>
        </div>
        {/* <div class={this.state.class} role="alert">
          Login successful!
        </div> */}
        <div className="login-parts">
          <div className="login-part-one">
            <div className="part-one-header">
              <div className="login-heading">
                <h4
                  className="login-hover"
                  onClick={() => {
                    this.props.changeLoginFormStatus();
                  }}
                >
                  Login
                </h4>
              </div>
              <div className="create-account-heading">
                <h4
                  className="create-account-hover"
                  onClick={() => {
                    this.props.changeSignupFormStatus();
                  }}
                >
                  Create Account
                </h4>
              </div>
            </div>
            {this.props.loginFormStatus && (
              <Login
                getDetails={this.props.getDetails}
                changeClass={this.changeClass}
              />
            )}
            {!this.props.loginFormStatus && (
              <CreateAccount storeDetails={this.props.storeDetails} />
            )}

            <div className="create-account"></div>
          </div>
          <div className="login-part-two">
            <h4>Check Order / Initiate return</h4>
            <p>
              See your order even if you are not a registered user. Enter the
              order number and phone number.
            </p>
            <div className="order-number">
              <input
                className="form-control me-1 bg-transparent p-2 rounded-0"
                type="search"
                placeholder="ORDER NUMBER*"
                aria-label="Search"
              />
            </div>
            <div className="order-phone">
              <input
                className="form-control me-1 bg-transparent p-2 rounded-0"
                type="search"
                placeholder="ORDER PHONE*"
                aria-label="Search"
              />
            </div>
            <a className="btn check-status rounded-0 p-2">CHECK</a>
          </div>
        </div>
      </div>
    );
  }
}

export default LoginAndSignup;
