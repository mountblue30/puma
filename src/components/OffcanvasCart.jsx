import React, { Component } from 'react';
import "../css/OffcanvasCart.css"
import emptycart from "../images/emptycart.jpg";
import whiteLock from "../images/white-lock.png";
import { Link } from "react-router-dom";
import OffcanvasCartProduct from "./OffcanvasCartProduct";

class OffcanvasCart extends Component {
  render() {
    return (
      <div
        className="offcanvas offcanvas-end"
        tabIndex="-1"
        id="offcanvasExample"
        aria-labelledby="offcanvasExampleLabel"
      >
        <div className="offcanvas-header">
          <h5 className="offcanvas-title" id="offcanvasExampleLabel">
            My Shopping Cart ({this.props.cart.length})
          </h5>
          <button
            type="button"
            className="btn-close"
            data-bs-dismiss="offcanvas"
            aria-label="Close"
          ></button>
        </div>
        <div className="offcanvas-body">
          {this.props.cart.length !== 0 && (
            <>
              <div className="offcanvas-products">
                {this.props.cart.map((product) => {
                  return (
                    <OffcanvasCartProduct
                      product={product}
                      updateQuantity={this.props.updateQuantity}
                      deleteItem={this.props.deleteItem}
                    />
                  );
                })}
              </div>
              <div className="offcanvas-checkout">
                <div className="shipping-cost">
                  <p>Shipping costs</p>
                  <h6>FREE</h6>
                </div>
                <div className="grand-total">
                  <div className="total">
                    <div className="total-title">
                      <h5>Grand Total</h5>
                      <span>Prices include GST</span>
                    </div>
                    <div className="price">
                      <strong>
                        {this.props.totalWithCommas(this.props.cartTotal)}
                      </strong>
                    </div>
                  </div>
                </div>
                <div className="checkouts">
                  <Link to="/checkout" className="offcanvas-secure-checkout">
                    
                      <img src={whiteLock} height="20" /> Secure Checkout
                  
                  </Link>

                  <Link to="/cart">
                    <div className="offcanvas-view-cart">View Cart</div>
                  </Link>
                </div>
              </div>
            </>
          )}
          {this.props.cart.length === 0 && (
            <div className="empty-cart-offcanvas">
              <div className="empty-cart-offcanvas-title">
                <h3>Cart is empty!</h3>
              </div>
              <div className="empty-cart-offcanvas-image">
                <img src={emptycart} />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
 
export default OffcanvasCart;