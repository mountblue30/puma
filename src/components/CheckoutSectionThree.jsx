import React, { Component } from 'react';
import CheckoutPopupModal from "./CheckoutPopupModal";

class CheckoutSectionThree extends Component {
    constructor(props) {
        super(props);
    }
    state = {  }
    render() { 
        return (
          <>
            <div className="section-one-part-three">
              <div className="section-one-part-three-header">
                <h5>3. REVIEW</h5>
              </div>
              <div className={this.props.reviewContent}>
                <p>
                  Please review your information is accurate. Your order will
                  not be placed until you click "Place Order".
                </p>
                <h3>
                  <strong>ORDER TOTAL ₹987.24</strong>
                </h3>
                <a
                  className="offcanvas-secure-checkout"
                  data-bs-toggle="modal"
                  data-bs-target="#staticBackdrop"
                >
                  PLACE ORDER
                </a>
                <p>
                  By submitting my order, I confirm that I have read and accept
                  the Terms and Conditions and the Privacy Policy.
                </p>
              </div>
            </div>
            <CheckoutPopupModal clearCart={this.props.clearCart} />
          </>
        );
    }
}
 
export default CheckoutSectionThree;