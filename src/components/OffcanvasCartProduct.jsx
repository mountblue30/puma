import React, { Component } from "react";


class OffcanvasCartProduct extends Component {
  constructor(props) {
    super(props);
  }
  state = {};
  render() {
    return (
      <>
        <div className="offcanvas-product desktop">
          <div className="image">
            <img src={this.props.product.image_primary} alt="" height="85" />
          </div>
          <div className="details">
            <p className="description">
              {this.props.product.description.slice(0, 60)}...
            </p>
            <p>Color: {this.props.product.color}</p>
            <p>Style: {this.props.product.style}</p>
            <div class="dropdown">
              <label>Quantity: </label>
              <a
                class="btn drop dropdown-toggle rounded-0 p-2"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                {this.props.product.quantity}
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a
                    class="dropdown-item"
                    name="1"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 1);
                    }}
                  >
                    1
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="2"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 2);
                    }}
                  >
                    2
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="3"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 3);
                    }}
                  >
                    3
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="4"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 4);
                    }}
                  >
                    4
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="5"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 5);
                    }}
                  >
                    5
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="pricing">
            <div className="price">
              <h6>{this.props.product.price}</h6>
              <button className="btn">
                <i
                  class="fa-solid fa-trash-can"
                  onClick={() => {
                    this.props.deleteItem(this.props.product);
                  }}
                ></i>
              </button>
            </div>
          </div>
        </div>
        <div className="offcanvas-product mobile">
          <div className="image">
            <img src={this.props.product.image_primary} alt="" height="85" />
          </div>
          <div className="details">
            <p className="description">
              {this.props.product.description.slice(0, 60)}...
            </p>
            <hr />
            <p>Color: {this.props.product.color}</p>
            <p>Style: {this.props.product.style}</p>
            <div class="dropdown">
              <label>Quantity: </label>
              <a
                class="btn drop dropdown-toggle"
                href="#"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                {this.props.product.quantity}
              </a>

              <ul class="dropdown-menu">
                <li>
                  <a
                    class="dropdown-item"
                    name="1"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 1);
                    }}
                  >
                    1
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="2"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 2);
                    }}
                  >
                    2
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="3"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 3);
                    }}
                  >
                    3
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="4"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 4);
                    }}
                  >
                    4
                  </a>
                </li>
                <li>
                  <a
                    class="dropdown-item"
                    name="5"
                    onClick={() => {
                      this.props.updateQuantity(this.props.product.id, 5);
                    }}
                  >
                    5
                  </a>
                </li>
              </ul>
            </div>
            <div className="pricing">
              <div className="price">
                <h6>{this.props.product.price}</h6>
                <button className="btn">
                  <i
                    class="fa-solid fa-trash-can"
                    onClick={() => {
                      this.props.deleteItem(this.props.product);
                    }}
                  ></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default OffcanvasCartProduct;
