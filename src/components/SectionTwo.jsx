import React, { Component } from "react";
import "../css/SectionTwo.css";
import frontImage from "../images/yellowBgFront.jpeg";
import backImage from "../images/yellowBGPic.jpg";

class SectionTwo extends Component {
  render() {
    return (
      <div className="section-two-container">
        <div className="yellow-card">
          <div className="content">
            <h1>
              PUMA X<br /> POKÉMON <br />
            </h1>
            <h3>GOTTA CATCH ’EM ALL! ™</h3>
            <button className="btn btn-warning">SHOP NOW</button>
          </div>
          <img className="back-image" src={backImage} alt="Bootstrap" />
        </div>
        <img className="front-image" src={frontImage} alt="Bootstrap" />
      </div>
    );
  }
}

export default SectionTwo;
