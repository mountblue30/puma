import React, { Component } from 'react';
import "../css/SearchResults.css"
import SearchResultProduct from './SearchResultProduct';

class SearchResults extends Component {
    state = {  } 
    render() { 
        return (
          <>
            <div className="results-found">
              <h1>7,142 PRODUCTS FOUND</h1>
            </div>
            <div className="result-products">
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
              <SearchResultProduct />
            </div>
          </>
        );
    }
}
 
export default SearchResults;