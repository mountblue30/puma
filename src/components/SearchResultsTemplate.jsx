import React, { Component } from "react";
import ProductTemplate from "./ProductTemplate";
import noProducts from "../images/productNotFound.png";


class SearchResultsTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: "",
      category: "",
      price: "",
      color: "",
      filteredProduct: "",
      searchTerm: "",
    };
  }
  componentDidUpdate(previousProps, previousState) {
    if (this.props.searchTerm !== previousState.searchTerm) {
      this.setState({
        searchTerm: this.props.searchTerm,
        gender: "",
        category: "",
        price: "",
        color: "",
        filteredProduct: this.props.productData.filter((product) => {
          return product.title.toLowerCase().includes(this.props.searchTerm);
        }),
      });
    } else {
      return;
    }
  }
  filter = (event) => {
    let gender = this.state.gender === "" ? false : this.state.gender;
    let category = this.state.category === "" ? false : this.state.category;
    let price = this.state.price === "" ? false : this.state.price;

    if (
      event.target.name === "male" ||
      event.target.name === "female" ||
      event.target.name === "both"
    ) {
      if (event.target.name === "both") {
        if (category && price) {
          if (price === "Lowest to highest") {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === category;
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === category;
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else if (!category && price) {
          if (price === "Lowest to highest") {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData.sort(
                (productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }
              ),
            });
          } else {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData.sort(
                (productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }
              ),
            });
          }
        } else if (category && !price) {
          this.setState({
            gender: event.target.name,
            filteredProduct: this.props.productData.filter((product) => {
              return product.category === category;
            }),
          });
        } else {
          this.setState({
            gender: event.target.name,
            filteredProduct: this.props.productData,
          });
        }
      } else {
        if (category && price) {
          if (price === "Lowest to highest") {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    product.gender === event.target.name &&
                    product.category === category
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    product.gender === event.target.name &&
                    product.category === category
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else if (!category && price) {
          if (price === "Lowest to highest") {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.gender === event.target.name;
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              gender: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.gender === event.target.name;
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else if (category && !price) {
          this.setState({
            gender: event.target.name,
            filteredProduct: this.props.productData.filter((product) => {
              return (
                product.gender === event.target.name &&
                product.category === category
              );
            }),
          });
        } else {
          console.log("one", event.target.name);
          this.setState({
            gender: event.target.name,
            filteredProduct: this.props.productData.filter((product) => {
              return product.gender === event.target.name;
            }),
          });
        }
      }
    }
    if (
      event.target.name === "t-shirt" ||
      event.target.name === "sweatshirt" ||
      event.target.name === "jacket"
    ) {
      if (gender && price) {
        if (gender === "both") {
          if (price === "Lowest to highest") {
            this.setState({
              category: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === event.target.name;
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              category: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === event.target.name;
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else {
          if (price === "Lowest to highest") {
            this.setState({
              category: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    product.gender === gender &&
                    product.category === event.target.name
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              category: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    (product.gender === gender && product.category) ===
                    event.target.name
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        }
      } else if (gender && !price) {
        if (gender === "both") {
          this.setState({
            category: event.target.name,
            filteredProduct: this.props.productData.filter((product) => {
              return product.category === event.target.name;
            }),
          });
        } else {
          this.setState({
            category: event.target.name,
            filteredProduct: this.props.productData.filter((product) => {
              return (
                product.gender === gender &&
                product.category === event.target.name
              );
            }),
          });
        }
      } else if (price && !gender) {
        if (price === "Lowest to highest") {
          this.setState({
            category: event.target.name,
            filteredProduct: this.props.productData
              .filter((product) => {
                return product.category === event.target.name;
              })
              .sort((productA, productB) => {
                return (
                  +productA.price.replace(",", "").replace("₹", "") -
                  +productB.price.replace(",", "").replace("₹", "")
                );
              }),
          });
        } else {
          this.setState({
            category: event.target.name,
            filteredProduct: this.props.productData
              .filter((product) => {
                return product.category === event.target.name;
              })
              .sort((productA, productB) => {
                return (
                  +productB.price.replace(",", "").replace("₹", "") -
                  +productA.price.replace(",", "").replace("₹", "")
                );
              }),
          });
        }
      } else {
        this.setState({
          category: event.target.name,
          filteredProduct: this.props.productData.filter((product) => {
            return product.category === event.target.name;
          }),
        });
      }
    }
    if (
      event.target.name === "Lowest to highest" ||
      event.target.name === "Highest to lowest"
    ) {
      if (event.target.name === "Lowest to highest") {
        if (gender && category) {
          if (gender === "both") {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === category;
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    product.gender === gender && product.category === category
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else if (gender && !category) {
          if (gender !== "both") {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.gender === gender;
                })
                .sort((productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData.sort(
                (productA, productB) => {
                  return (
                    +productA.price.replace(",", "").replace("₹", "") -
                    +productB.price.replace(",", "").replace("₹", "")
                  );
                }
              ),
            });
          }
        } else if (category && !gender) {
          this.setState({
            price: event.target.name,
            filteredProduct: this.props.productData
              .filter((product) => {
                return product.category === category;
              })
              .sort((productA, productB) => {
                return (
                  +productA.price.replace(",", "").replace("₹", "") -
                  +productB.price.replace(",", "").replace("₹", "")
                );
              }),
          });
        } else {
          this.setState({
            price: event.target.name,
            filteredProduct: this.props.productData.sort(
              (productA, productB) => {
                return (
                  +productA.price.replace(",", "").replace("₹", "") -
                  +productB.price.replace(",", "").replace("₹", "")
                );
              }
            ),
          });
        }
      } else {
        if (gender && category) {
          if (gender === "both") {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.category === category;
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return (
                    product.gender === gender && product.category === category
                  );
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          }
        } else if (gender && !category) {
          if (gender !== "both") {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData
                .filter((product) => {
                  return product.gender === gender;
                })
                .sort((productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }),
            });
          } else {
            this.setState({
              price: event.target.name,
              filteredProduct: this.props.productData.sort(
                (productA, productB) => {
                  return (
                    +productB.price.replace(",", "").replace("₹", "") -
                    +productA.price.replace(",", "").replace("₹", "")
                  );
                }
              ),
            });
          }
        } else if (category && !gender) {
          this.setState({
            price: event.target.name,
            filteredProduct: this.props.productData
              .filter((product) => {
                return product.category === category;
              })
              .sort((productA, productB) => {
                return (
                  +productB.price.replace(",", "").replace("₹", "") -
                  +productA.price.replace(",", "").replace("₹", "")
                );
              }),
          });
        } else {
          this.setState({
            price: event.target.name,
            filteredProduct: this.props.productData.sort(
              (productA, productB) => {
                return (
                  +productB.price.replace(",", "").replace("₹", "") -
                  +productA.price.replace(",", "").replace("₹", "")
                );
              }
            ),
          });
        }
      }
    }
  };
  render() {
    return (
      <>
        <div className="search-results">
          <div className="search-results-header">
            <h5>Home</h5>
            <p>.</p>
            <h5>Search</h5>
            <p>.</p>
            <h6>mens</h6>
          </div>
          <div className="sort-search-results">
            <div className="part-one">
              <div className="sort-icon">
                <i class="fa-solid fa-filter"></i>
              </div>
              <div class="dropdown">
                <a
                  class="btn drop dropdown-toggle rounded-0 p-2"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  {this.state.gender === "" && "Gender"}
                  {this.state.gender !== "" && this.state.gender}
                </a>

                <ul class="dropdown-menu">
                  <li>
                    <a
                      class="dropdown-item"
                      name="male"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Male
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="female"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Female
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="both"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Both
                    </a>
                  </li>
                </ul>
              </div>
              <div class="dropdown">
                <a
                  class="btn drop dropdown-toggle rounded-0 p-2"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  {this.state.category === "" && "Category"}
                  {this.state.category !== "" && this.state.category}
                </a>

                <ul class="dropdown-menu">
                  <li>
                    <a
                      class="dropdown-item"
                      name="t-shirt"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      T-Shirts
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="sweatshirt"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Sweatshirts
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="jacket"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Jackets
                    </a>
                  </li>
                </ul>
              </div>
              <div class="dropdown">
                <a
                  class="btn drop dropdown-toggle rounded-0 p-2"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  {this.state.price === "" && "Price"}
                  {this.state.price !== "" && this.state.price}
                </a>

                <ul class="dropdown-menu">
                  <li>
                    <a
                      class="dropdown-item"
                      name="Lowest to highest"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Lowest to highest
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="Highest to lowest"
                      onClick={(event) => {
                        this.filter(event);
                      }}
                    >
                      Highest to lowest
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          {this.props.productData.length === 0 &&
            this.state.filteredProduct.length === 0 &&
            this.props.searchTerm === "" && (
              <div className="no-products-found">
                <h3>No Products found!</h3>
                <img src={noProducts} alt="" />
              </div>
            )}
          {this.props.productData.length === 0 &&
            this.state.filteredProduct.length === 0 &&
            this.props.searchTerm !== "" && (
              <div className="no-products-found">
                <h3>No Products found!</h3>
                <img src={noProducts} alt="" />
              </div>
            )}

          {this.props.productData.length !== 0 &&
            this.state.filteredProduct.length === 0 &&
            this.props.searchTerm !== "" && (
              <div className="no-products-found">
                <h3>No Products found!</h3>
                <img src={noProducts} alt="" />
              </div>
            )}
          {this.state.filteredProduct === "" &&
            this.props.productData.length !== 0 && (
              <ProductTemplate
                productData={this.props.productData}
                addToCart={this.props.addToCart}
                deleteItem={this.props.deleteItem}
              />
            )}
          {this.state.filteredProduct !== "" && (
            <ProductTemplate
              productData={this.state.filteredProduct}
              addToCart={this.props.addToCart}
              deleteItem={this.props.deleteItem}
            />
          )}
        </div>
      </>
    );
  }
}

export default SearchResultsTemplate;
