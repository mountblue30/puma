import React, { Component } from "react";
import "../css/CheckoutPage.css";

import CheckoutSectionOne from "./CheckoutSectionOne";
import CheckoutSectionTwo from "./CheckoutSectionTwo";
import CheckoutSectionThree from "./CheckoutSectionThree";
import CheckoutCollapse from "./CheckoutCollapse";
import notLoggedIn from "../images/notLoggedIn.jpg"
class CheckoutPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sectionOnePartOneInputs: "section-one-part-one-inputs",
      sectionOnePartOneContent: "section-one-part-one-contents",
      paymentChecks: "payment-checks",
      sectionTwoContent: "section-one-part-two-contents",
      reviewContent: "review-contents",
      address: "address",
      pay:"pay",
      review:"review"
    };
  }
  flipClass = (part,action) => {
    if (part === "part-one" && action === "submit") {
      this.setState({
        sectionOnePartOneInputs: "section-one-part-one-inputs disable",
        sectionOnePartOneContent: "section-one-part-one-contents active ",
        paymentChecks: "payment-checks active",
        address:"address active"
      });
    } else if(part === "part-one" && action === "edit"){
      this.setState({
        sectionOnePartOneInputs: "section-one-part-one-inputs ",
        sectionOnePartOneContent: "section-one-part-one-contents",
        paymentChecks: "payment-checks",
        sectionTwoContent: "section-one-part-two-contents",
        reviewContent: "review-contents",
        address: "address",
        pay: "pay",
      });
    } else if(part === "part-two" && action === "submit"){
      this.setState({
        paymentChecks: "payment-checks",
        sectionTwoContent: "section-one-part-two-contents active",
        reviewContent: "review-contents active",
        pay:"pay active"
      });
    } else if(part === "part-two" && action === "edit"){
      this.setState({
        paymentChecks: "payment-checks active",
        sectionTwoContent: "section-one-part-two-contents",
        reviewContent: "review-contents",
        pay: "pay",
      });
    }
  };
  render() {
    return (
      <>
        <div className="checkout-container">
          {this.props.loggedIn && (
            <>
              <div className="checkout-header">
                <h3>
                  <strong>CHECKOUT</strong>
                </h3>
              </div>
              <div className="checkout-sections">
                <div className="checkout-section-one">
                  <CheckoutSectionOne
                    sectionOneInput={this.state.sectionOnePartOneInputs}
                    sectionOneContent={this.state.sectionOnePartOneContent}
                    flipClass={this.flipClass}
                    address={this.state.address}
                  />
                  <CheckoutSectionTwo
                    paymentChecks={this.state.paymentChecks}
                    sectionTwoContent={this.state.sectionTwoContent}
                    flipClass={this.flipClass}
                    pay={this.state.pay}
                  />
                  <CheckoutSectionThree
                    reviewContent={this.state.reviewContent}
                    review={this.props.review}
                    clearCart={this.props.clearCart}
                  />
                </div>
                <div className="checkout-section-two">
                  <p>
                    <button
                      class="btn w-100 d-flex justify-content-between align-items-center "
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseWidthExample"
                      aria-expanded="true"
                      aria-controls="collapseWidthExample"
                    >
                      <h5>ORDER DETAILS ({this.props.cart.length}) </h5>

                      <i class="fa-solid fa-angle-down mt-"></i>
                    </button>
                  </p>
                  <CheckoutCollapse
                    cart={this.props.cart}
                    updateQuantity={this.props.updateQuantity}
                    deleteItem={this.props.deleteItem}
                    cartTotal={this.props.cartTotal}
                    totalWithCommas={this.props.totalWithCommas}
                  />
                </div>
              </div>
            </>
          )}
          {!this.props.loggedIn && (
            <div className="not-loggedin">
              <h3>Login to proceed checkout.</h3>
              <div className="not-loggedin-image">
                <img src={notLoggedIn} alt="" />
              </div>
            </div>
          )}
        </div>
      </>
    );
  }
}

export default CheckoutPage;
