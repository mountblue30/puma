import React, { Component } from 'react';
import validator from "validator";

class CreateAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      firstNameError: "",
      lastNameError: "",
      emailError: "",
      passwordError: "",
      signupError: "",
      signupSuccess: "",
    };
  }
  handleInputChange = (event) => {
    if (event.target.name === "email") {
      this.setState({
        email: event.target.value,
      });
    } else if (event.target.name === "firstname") {
      this.setState({
        firstName: event.target.value,
      })
    } else if (event.target.name === "lastname") {
      this.setState({
        lastName: event.target.value,
      })
    } else {
        this.setState({
          password: event.target.value,
        });
    }
  };
  validate = () => {
    let firstNameError = "";
    let lastNameError = "";
    let emailError = "";
    let passwordError = "";
    if (validator.isEmpty(this.state.firstName)) {
      firstNameError = "Firstname should not be empty";
    }
    if (validator.isEmpty(this.state.lastName)) {
      lastNameError = "Lastname should not be empty";
    }
    if (validator.isEmpty(this.state.email)) {
      emailError = "Email should not be empty";
    } else {
      if (!validator.isEmail(this.state.email)) {
        emailError = "Provide a valid email address.";
      }
    }
    if (validator.isEmpty(this.state.password)) {
      passwordError = "Password should not be empty.";
    } else {
      if (!validator.isStrongPassword(this.state.password)) {
        passwordError = "Please provide a string password.";
      }
    }
    if (firstNameError|| lastNameError || emailError || passwordError) {
      this.setState({ firstNameError,lastNameError, emailError, passwordError });
      return false;
    }
    this.setState({ firstNameError, lastNameError, emailError, passwordError });
    return true;
  };
  submit = () => {

    if(this.validate()){
      let result = this.props.storeDetails(this.state);
      if (result["status"]) {
        this.setState({
          signupSuccess: result["value"],
          signupError:"",
        });
      } else {
        this.setState({
          signupError: result["value"],
          signupSuccess:"",
        });
      }
    }
  };
  render() {
    return (
      <div className="login">
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="search"
          placeholder="FIRST NAME*"
          name="firstname"
          onChange={this.handleInputChange}
        />
        <div className="errorMsg">{this.state.firstNameError}</div>
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="search"
          placeholder="LAST NAME*"
          name="lastname"
          onChange={this.handleInputChange}
        />
        <div className="errorMsg">{this.state.lastNameError}</div>
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="search"
          placeholder="EMAIL*"
          name="email"
          onChange={this.handleInputChange}
        />
        <div className="errorMsg">{this.state.emailError}</div>
        <input
          className="form-control me-1 bg-transparent p-2 rounded-0"
          type="password"
          placeholder="PASSWORD*"
          name="password"
          onChange={this.handleInputChange}
        />
        <div className="errorMsg">{this.state.passwordError}</div>
        <div className="create-account-submit">
          <a
            className="btn login-button rounded-0 p-2"
            onClick={() => {
              this.submit();
            }}
          >
            CREATE ACCOUNT
          </a>
        </div>
        {/* <div className="successMsg">{this.state.signupSuccess}</div> */}
        <div className="errorMsg">{this.state.signupError}</div>
      </div>
    );
  }
}
 
export default CreateAccount;