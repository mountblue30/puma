import React, { Component } from 'react';
import dressOne from "../images/Dress1.jpg"


class CartProduct extends Component {
    constructor(props){
      super(props);
     
    }

    render() { 
        return (
          <div className="cart-product">
            <div className="image">
              <img
                src={this.props.product.image_primary}
                alt=""
                height="300"
                className="product-image"
              />
            </div>
            <div className="cart-details">
              <h5>{this.props.product.description.slice(0, 100)}...</h5>
              <p>Color: {this.props.product.color}</p>
              <p>Style: {this.props.product.style}</p>
              <div class="dropdown">
                <label>Quantity: </label>
                <a
                  class="btn drop dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  {this.props.product.quantity}
                </a>

                <ul class="dropdown-menu">
                  <li>
                    <a
                      class="dropdown-item"
                      name="1"
                      onClick={() => {
                        this.props.updateQuantity(this.props.product.id, 1);
                      }}
                    >
                      1
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="2"
                      onClick={() => {
                        this.props.updateQuantity(this.props.product.id, 2);
                      }}
                    >
                      2
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="3"
                      onClick={() => {
                        this.props.updateQuantity(this.props.product.id, 3);
                      }}
                    >
                      3
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="4"
                      onClick={() => {
                        this.props.updateQuantity(this.props.product.id, 4);
                      }}
                    >
                      4
                    </a>
                  </li>
                  <li>
                    <a
                      class="dropdown-item"
                      name="5"
                      onClick={() => {
                        this.props.updateQuantity(this.props.product.id, 5);
                      }}
                    >
                      5
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="pricing">
              <div className="price">
                <h6>{this.props.product.price}</h6>
                <button className="btn">
                  <i
                    class="fa-solid fa-trash-can"
                    onClick={() => {
                      this.props.deleteItem(this.props.product);
                    }}
                  ></i>
                </button>
              </div>
            </div>
          </div>
        );
    }
}
 
export default CartProduct;