import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../css/Products.css";
class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: "",
    };
  }
  changeImage = (image) => {
    this.setState({
      currentImage: image,
    });
  };
  render() {
    return (
      <>
        <div className="result-product">
          <Link
            className="product-link"
            to={`/product/${this.props.product.id}`}
          >
            {this.state.currentImage !== "" &&
              this.state.currentImage !== this.props.product.image_primary &&
              this.state.currentImage !==
                this.props.product.image_secondary && (
                <img
                  className="result-product-image"
                  src={this.props.product.image_primary}
                  onClick={() => {
                    const section = document.querySelector("#scrollTo");
                    section.scrollIntoView({
                      behavior: "smooth",
                      block: "start",
                    });
                  }}
                />
              )}
            {this.state.currentImage !== "" && (
              <img
                className="result-product-image"
                src={this.state.currentImage}
              />
            )}
            {this.state.currentImage === "" && (
              <img
                className="result-product-image"
                src={this.props.product.image_primary}
              />
            )}
          </Link>
          <div className="color">Color: {this.props.product.color}</div>
          <div className="optional-images">
            <a
              onClick={() => {
                this.changeImage(this.props.product.image_primary);
              }}
            >
              <img src={this.props.product.image_primary} alt="" height="70" />
            </a>
            <a
              onClick={() => {
                this.changeImage(this.props.product.image_secondary);
              }}
            >
              <img
                src={this.props.product.image_secondary}
                alt=""
                height="70"
              />
            </a>
          </div>
          <div className="result-product-content">
            <Link
              className="product-link"
              to={`/product/${this.props.product.id}`}
            >
              <div
                className="product-title-price"
                onClick={() => {
                  const section = document.querySelector("#scrollTo");
                  section.scrollIntoView({
                    behavior: "smooth",
                    block: "start",
                  });
                }}
              >
                <p>{this.props.product.title.slice(0, 30)}...</p>
                <h6>{this.props.product.price}</h6>
              </div>
              <div className="coupon">
                <p>Use code: MORE10 for extra 10% off</p>
              </div>
            </Link>
          </div>
        </div>
      </>
    );
  }
}

export default Products;
