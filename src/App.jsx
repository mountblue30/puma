// React
import React, { Component } from "react";

// React-route
import { Route, Switch,Redirect } from "react-router-dom";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Cart from "./components/Cart";
import MainContent from "./components/MainContent";
import LoginAndSignup from "./components/LoginAndSignup";
import SearchResultsTemplate from "./components/SearchResultsTemplate";
import SingleProductPage from "./components/SingleProductPage";
import NewArrivals from "./components/NewArrivals";
import CheckoutPage from "./components/CheckoutPage";
import toast, { Toaster } from "react-hot-toast";

let productData = require("./product_data.json");


// Components
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginFormStatus: true,
      user: {},
      filteredData: "",
      searchTerm: "",
      cart: [],
      cartTotal: 0,
      loggedIn: false,
      class: "collapse in",
    };
    this.testRef = React.createRef();
  }
  scrollToElement = () => this.testRef.current.scrollIntoView();
  // -------------------------------------------------- Cart functionality ---------------------------------------------
  getExistingProduct = (id) => {
    return this.state.cart.findIndex((product) => product.id === id);
  };
  getOtherProducts = (id) => {
    return this.state.cart.filter((item) => {
      return item.id !== id;
    });
  };
  toggleCartPopup = () => {
    if (this.state.showCartPopup) {
      this.setState((state) => {
        return {
          showCartPopup: !state.showCartPopup,
        };
      });
    } else {
      this.setState(
        (state) => {
          return {
            showCartPopup: !state.showCartPopup,
          };
        },
        () => {
          setTimeout(() => {
            this.toggleCartPopup();
          }, 1000);
        }
      );
    }
  };
  totalWithCommas = (total) => {
    return `₹ ${total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",").replace("-","")}`;
  };
  updateQuantity = (productId, quantity) => {
    console.log(productId, quantity);
    let productIndex = this.getExistingProduct(productId);
    let product = this.state.cart[productIndex];
    let price = +product["price"].replace("₹", "").replace(",", "");
    product["prevQuantity"] = product["quantity"];
    product["quantity"] = quantity;
    product["currentPrice"] = price * product["quantity"];
    this.setState((prevState) => {
      return {
        cart: [
          ...prevState.cart.slice(0, productIndex),
          product,
          ...prevState.cart.slice(productIndex + 1),
        ],
        cartTotal:
          prevState.cartTotal -
          product["prevQuantity"] * price +
          product["currentPrice"],
      };
    });
  };
  addToCart = (product, quantity) => {
    let price = +product["price"].replace("₹", "").replace(",", "");
    if (!quantity) {
      let checkExistingProduct = this.getExistingProduct(product.id);
      if (checkExistingProduct !== -1) {
        this.updateQuantity(product.id, product.quantity + 1);
      } else {
        product["quantity"] = 1;
        product["prevQuantity"] = 1;
        product["currentPrice"] = price;
        this.setState(
          (prevState) => ({
            cart: [...prevState.cart, product],
            cartTotal: prevState.cartTotal + price,
          }),
          () => {
            this.toggleCartPopup();
            console.log("new", this.state.cart);
          }
        );
      }
    } else {
      product["quantity"] = quantity;
      product["prevQuantity"] = 1;
      product["currentPrice"] = price;
      let checkExistingProduct = this.getExistingProduct(product.id);
      if (checkExistingProduct !== -1) {
        this.updateQuantity(product.id, product.quantity);
      } else {
        this.setState(
          (prevState) => ({
            cart: [...prevState.cart, product],
            cartTotal: prevState.cartTotal + price * quantity,
          }),
          () => {
            this.toggleCartPopup();
            console.log("new", this.state.cart);
          }
        );
      }
    }
    
    toast.success("Product added to cart", { position: "top-center" });
  };
  deleteItem = (product) => {
    let productIndex = this.getExistingProduct(product.id);
    let price = +product["price"].replace("₹", "").replace(",", "");
    this.setState(
      (prevState) => {
        return {
          cartTotal: prevState.cartTotal - price * product["quantity"],
          cart: [
            ...prevState.cart.slice(0, productIndex),
            ...prevState.cart.slice(productIndex + 1),
          ],
        };
      },
      () => {
        toast.success("Product removed from the cart.", {
          position: "top-center",
        });

      }
    );
  };
  // ------------------------------------------------- Cart functionality -----------------------------------------------

  changeLoginFormStatus = () => {
    this.setState((state) => {
      if (!state.loginFormStatus) {
        return {
          loginFormStatus: true,
        };
      }
    });
  };

  changeSignupFormStatus = () => {
    this.setState((state) => {
      if (state.loginFormStatus) {
        return {
          loginFormStatus: false,
        };
      }
    });
  };
  onSearch = (value) => {
    if (value !== "") {
      this.setState(
        {
          searchTerm: value,
          filteredData: productData.filter((product) => {
            return product.title.toLowerCase().includes(value.toLowerCase());
          }),
        },
        () => {
          console.log("current data", this.state.filteredData);
        }
      );
    } else {
      this.setState({
        searchTerm: value,
        filteredData: [],
      });
    }
  };
  storeDetails = (data) => {
    if (!this.state.user[data["email"]]) {
      this.setState(
        (state) => {
          let tempUser = state.user;
          tempUser[data["email"]] = {
            firstName: data["firstName"],
            lastName: data["lastName"],
            email: data["email"],
            password: data["password"],
          };
          return {
            user: tempUser,
          };
        },
        () => {
          toast.success("Signup successful!", { position: "top-center" });
          setTimeout(() => {
            this.setState({
              loginFormStatus: true,
            });
          

          }, 1000);
        }
      );

      return { value: "Signup successful.", status: true };
    } else {
      return { value: "User is already present", status: false };
    }
  };
  getDetails = (data) => {
    if (!this.state.user[data["email"]]) {
      return { value: "User not present.", status: false };
    } else {
      if (this.state.user[data["email"]]["password"] === data["password"]) {
        setTimeout(() => {
          this.setState({
            loggedIn: true,
          });
        }, 1500);
        toast.success("Login successful!", { position: "top-center" });

        return { value: "Login successful.", status: true };
      } else {
        return { value: "Password doesn't match.", status: false };
      }
    }
  };
  clearCart = () => {
    this.setState({
      cart: [],
    });
  };
  logOut = () => {
    this.setState({
      loggedIn:false
    })
  }
  render() {
    return (
      <>
        <Navbar
          onSearch={this.onSearch}
          cart={this.state.cart}
          updateQuantity={this.updateQuantity}
          deleteItem={this.deleteItem}
          cartTotal={this.state.cartTotal}
          totalWithCommas={this.totalWithCommas}
          logOut = {this.logOut}
          loggedIn = {this.state.loggedIn}
        />
        <div className="scrollTo" id="scrollTo" ref={this.testRef}>
          <Toaster/>
        </div>
        <Switch>
          <Route path="/" exact component={MainContent} />
          <Route
            path="/cart"
            render={(props) => (
              <Cart
                {...props}
                cart={this.state.cart}
                updateQuantity={this.updateQuantity}
                deleteItem={this.deleteItem}
                cartTotal={this.state.cartTotal}
                totalWithCommas={this.totalWithCommas}
                loggedIn={this.state.loggedIn}
              />
            )}
          />
          <Route
            path="/search"
            exact
            render={(props) => (
              <SearchResultsTemplate
                productData={this.state.filteredData}
                addToCart={this.addToCart}
                searchTerm={this.state.searchTerm}
                {...props}
              />
            )}
          />
          <Route
            path="/new-arrivals"
            exact
            render={(props) => (
              <NewArrivals
                productData={productData}
                addToCart={this.addToCart}
                deleteItem={this.deleteItem}
                {...props}
              />
            )}
          />
          <Route
            path="/product/:id"
            exact
            render={(props) => (
              <>
                <SingleProductPage
                  productData={
                    productData.filter((data) => {
                      return data.id === +props.match.params.id;
                    })[0]
                  }
                  addToCart={this.addToCart}
                  cart={this.state.cart}
                  cartTotal={this.state.cartTotal}
                  totalWithCommas={this.totalWithCommas}
                  loggedIn={this.state.loggedIn}
                  {...props}
                />
              </>
            )}
          />
          {this.state.loggedIn && <Redirect exact from="/login" to="/" />}
          <Route
            path="/login"
            exact
            render={(props) => (
              <LoginAndSignup
                loginFormStatus={this.state.loginFormStatus}
                changeLoginFormStatus={this.changeLoginFormStatus}
                changeSignupFormStatus={this.changeSignupFormStatus}
                storeDetails={this.storeDetails}
                getDetails={this.getDetails}
                {...props}
              />
            )}
          />
          <Route
            path="/checkout"
            exact
            render={(props) => (
              <CheckoutPage
                cart={this.state.cart}
                updateQuantity={this.updateQuantity}
                deleteItem={this.deleteItem}
                cartTotal={this.state.cartTotal}
                totalWithCommas={this.totalWithCommas}
                clearCart={this.clearCart}
                loggedIn={this.state.loggedIn}
                {...props}
              />
            )}
          />
        </Switch>
        <Footer />
      </>
    );
  }
}

export default App;
